<?php

require_once 'htmlpurifier/library/HTMLPurifier.auto.php';


/** Prepare timestamp for MySQL insertion */
function mydate($timestamp=0) {
	if(empty($timestamp)) { $timestamp = time(); }
	if(!is_numeric($timestamp)) { $timestamp = strtotime($timestamp); }
	return date("Y-m-d H:i:s",$timestamp);
}

/** Prepare timestamp for nice display */
function nicedate($timestamp=0) {
	if(empty($timestamp)) { $timestamp = time(); }
	if(!is_numeric($timestamp)) { $timestamp = strtotime($timestamp); }
	return date("l jS \of F Y H:i:s",$timestamp);
}

//Time-attack resistant hash comparason. Required for earlier versions of PHP. reference in comment - http://php.net/manual/en/function.hash-equals.php#115635
function my_hash_equals($str1, $str2)
{
	if(function_exists('hash_equals'))
	{
		return hash_equals($str1, $str2);
	}
	else
	{
	    if(strlen($str1) != strlen($str2))
	    {
	        return false;
	    }
	    else
	    {
	        $res = $str1 ^ $str2;
	        $ret = 0;
	        for($i = strlen($res) - 1; $i >= 0; $i--)
	        {
	            $ret |= ord($res[$i]);
	        }
	        return !$ret;
	    }
	}
}

/** HTML escape all content (output)*/
function h($text) {
	return htmlspecialchars($text);
}

/** HTML escape all content in array (output)*/
function hArray($textArray)
{
	foreach($textArray as &$arr)
	{
		$arr = h($arr);
	}
	return $textArray;
}

//** HTML remove and escape dangerous content (input)*/
// While HTML purifier does remove unwanted input it also encodes characters - Should not be used when storing followed by futher escaping. 
function c($text) {
	$conf = HTMLPurifier_Config::createDefault();
	$conf->set('Core.Encoding', 'ISO-8859-1');
	$conf->set('HTML.Doctype', 'HTML 4.01 Transitional');
	$conf->set('CSS.Trusted', true);
	$conf->set('HTML.Allowed','a[href|title],b,em,strong,cite,blockquote,code,s,sub,sup,ul,u,ol,li,dl,dt,dd,p,br,h1,h2,h3,h4,h5,h6,span,*[style]');
	$conf->set('Core.RemoveProcessingInstructions','true'); //This appears to omit automatic closing tags and will not remove "<?php" without a closing tag. 

	$purifier = new HTMLPurifier($conf);
	return $purifier->purify($text);
}

//** Alternate Input validation method - removing all html tags*/
function stripInput($text)
{
	$allowedTags = '';
	return strip_tags($text, $allowedTags);
}

//Input validation function to be used for DisplayName, Username, Email, Subject, Category, 
function testInput($name, $text)
{
	$result = array("name" => $name, "msg");
	if ($text == "") //Check input is not missing
	{
		$result['msg'] = $result['name']." is required"; 
		return $result;
	}
	if ($text != strip_tags($text)) //Check for html tags
	{
		$result['msg'] = $result['name']." contains illegal characters";
		return $result;
	}

}

/** add status message that this request is invalid and redirect to specified page or  home page */
function errorRedirect($f3, $message = "Invalid Request", $path = '/') {
	StatusMessage::add($message, "danger");
	return $f3->reroute($path);
}


/** Declare constants */
if (isset($_SERVER['BASE'])) { define('BASE',$_SERVER['BASE']); } else { define('BASE','/'); }

?>
