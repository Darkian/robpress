<?php

class File {

	public static function Upload($array,$local=false) {
		$f3 = Base::instance();
		extract($array);
		$directory = getcwd() . '/uploads';
		$destination = $directory . '/' . $name;
		$webdest = '/uploads/' . $name;

		//Local files get moved
		if($local) {
			if (copy($tmp_name,$destination)) {
				chmod($destination,0666);
				return $webdest;
			} else {
				return false;
			}
		//POSTed files are done with move_uploaded_file
		} else {
			$counter = 0;
			$allowed_exts_mimes=array("txt"=>"text/plain","png"=>"image/png","jpe"=>"image/jpeg","jpg"=>"image/jpeg","jpeg"=>"image/jpeg","gif"=>"image/gif","bmp"=>"image/bmp","pdf"=>"application/pdf","doc"=>"application/msword","docx"=>"application/msword"); //mapping whitelisted file extensions with associated (whitelisted) MIME types
			$file_ext=pathinfo($name, PATHINFO_EXTENSION);
			foreach($allowed_exts_mimes as $k=>$v) { //looping for each element in the associative array
				if(strcmp($file_ext, $k)==0 && $filesize <=10485760 && !strpos($k, '.') && !strpos($name, '.')){ //if the uploading file doesn't have illegal extensions AND isn't larger that 10MB
					if (move_uploaded_file($tmp_name,$destination)) {
						chmod($destination,0666);
						if (function_exists('finfo_open')) {
					        $finfo = finfo_open(FILEINFO_MIME_TYPE);
					        $mimetype = finfo_file($finfo, $destination);
					        finfo_close($finfo);
					    } else {
					        $mimetype = mime_content_type($destination);
					    }
					    if(in_array($mimetype, array($v))) {
					    	StatusMessage::add('File uploaded successfully.','info');
							return $webdest;
					    } else {
							if(unlink($destination)) StatusMessage::add('File not supported: MIME type not allowed.','danger');
							return false;
						}
					} else {
						if(unlink($destination)) StatusMessage::add('File not supported: failed to upload file.','danger');
						return false;
					}
				} else {
					if($counter==sizeof($allowed_exts_mimes)-1) {
						StatusMessage::add('File not supported: wrong file format.','danger');
						return false;
					} else {
						$counter=$counter+1;
						continue;
					}
				}
			}
		}
	}

}

?>
