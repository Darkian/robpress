<?php

class Form {

	public function __construct() {
	}

	public function start($options=array()) {
		$action = isset($options['action']) ? $options['action'] : '';
		$enctype = (isset($options['type']) && $options['type'] == 'file') ? 'enctype="multipart/form-data"' : ''; //Handle file uploads
		return '<form role="form" method="post" action="'.$action.'" '.$enctype.'>';	
	}

	public function file($options) {
		return '<input type="file" class="form-control" id="' . $options['field'] . '" name="' . $options['field'] . '" placeholder="' . $options['placeholder'] . '" value="' . $options['value'] . '">';
	}

	public function checkbox($options) {
		$checked = (isset($options['value']) && !empty($options['value'])) ? 'checked="checked"' : '';
			$output = '
				<div class="checkbox">
				<label>
				<input type="checkbox" name="'.$options['field'].'" '.$checked.' value="1">'.$options['label'].'
				</label>
				</div>';
		return $output;
	}

	public function select($options) {
		$output = '<select class="form-control" id="' . $options['field'] . '" name="' . $options['field'] . '">'; 
		foreach($options['items'] as $value=>$label) {
			$checked = ($options['value'] == $value) ? 'selected="selected"' : '';
			$output .= '<option value="'.$value.'" '.$checked.'>'.$label.'</option>';
		}
		$output .= '</select>';
		return $output;
	}


	public function checkboxes($options) {
		$output = '';	
		foreach($options['items'] as $value=>$label) {
			$checked = (is_array($options['value']) && in_array($value,$options['value'])) ? 'checked="checked"' : '';
			$output .= '
				<div class="checkbox">
				<label>
				<input type="checkbox" name="'.$options['field'].'[]" '.$checked.' value="'.$value.'">'.$label.'
				</label>
				</div>';
		}
		return $output;
	}

	public function hidden($options) {
		return '<input type="hidden" id="' . $options['field'] . '" name="' . $options['field'] . '" value="' . $options['value'] . '">';
	}

	public function text($options) {
		return '<input type="text" class="form-control" id="' . $options['field'] . '" name="' . $options['field'] . '" placeholder="' . $options['placeholder'] . '" value="' . $options['value'] . '">';
	}

	public function datetime($options) {
		return '<input type="text" class="datetime form-control" id="' . $options['field'] . '" name="' . $options['field'] . '" placeholder="' . $options['placeholder'] . '" value="' . $options['value'] . '">';
	}

	public function textarea($options) {
		return '<textarea style="height: 200px" class="form-control" id="' . $options['field'] . '" name="' . $options['field'] . '">' . $options['value'] . '</textarea>';
	}

	public function wysiwyg($options) {
		$f3 = Base::instance();
		$base = $f3->get('site.base');
		return '<textarea style="height: 200px" class="wysiwyg form-control" id="' . $options['field'] . '" name="' . $options['field'] . '">' . $options['value'] . '</textarea>
		<script type="text/javascript">CKEDITOR.replace(\'' . $options['field'] . "', {
toolbarGroups: [
 		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'colors' },
 		{ name: 'links' },
 		{ name: 'insert' },
	],
	filebrowserUploadUrl: '$base/utility/upload.php'
}


);</script>
		";
	}

	public function password($options) {
		return '<input type="password" class="form-control" id="' . $options['field'] . '" name="' . $options['field'] . '" placeholder="' . $options['placeholder'] . '" value="' . $options['value'] . '">';
	}

	public function submit($options) {
		if(!isset($options['class'])) { $options['class'] = 'btn-primary'; }
		return '<input type="submit" class="btn '.$options['class'].'" id="' . $options['field'] . '" name="' . $options['field'] . '" value="' . $options['label'] . '">';
	}

	public function end() {
		$csrfHelper = new CsrfHelper();
		$csrf = '<input type="hidden" name="csrfToken" value="'.$csrfHelper->prepareToken().'" />';
		return $csrf . '</form>';
	}

	public function add($field,$options=array()) {
		$options['label'] = $label = isset($options['label']) ? $options['label'] : ucfirst($field);
		$type = isset($options['type']) ? $options['type'] : 'text';
		if(isset($options['value'])) { $options['value'] = $options['value']; }
		else if(isset($_POST[$field])) { $options['value'] = $_POST[$field]; }
		elseif(!isset($options['value']) && isset($options['default'])) { $options['value'] = $options['default']; }
		else { $options['value'] = ''; }

		$options['field'] = $field;
		if(!isset($options['placeholder'])) { $options['placeholder'] = ''; }

		if(in_array($type,array('submit','hidden')) || (isset($options['div']) && $options['div'] == 0)) {
			return $this->$type($options);
		}

		$input = $this->$type($options);
		$result = <<<EOT
<div class="form-group">
<label for="$field">$label</label>
$input 
</div>	
EOT;
		return $result;
	}


	public function captcha($options){
	  // Adapted for The Art of Web: www.the-art-of-web.com
	  // Please acknowledge use of this code by including this header.

	  // Output image as base64 encoded text and remove the file directly afterwards. 

	  // initialise image with dimensions of 120 x 30 pixels
	  $f3 = Base::instance();


	  $image = @imagecreatetruecolor(120, 30) or die("Cannot Initialize new GD image stream");

	  // set background to white and allocate drawing colours
	  $background = imagecolorallocate($image, 0x22, 0x3E, 0x3C);
	  imagefill($image, 0, 0, $background);
	  //Spice up dem colours a bit 
	  $linecolor = imagecolorallocate($image, 0xCC, 0xCC, 0xCC);
	  $textcolor1 = imagecolorallocate($image, 0x75, 0xD1, 0x86);
	  $textcolor2 = imagecolorallocate($image, 0xF2, 0xCB, 0x4C);
  	  $textcolor3 = imagecolorallocate($image, 0xE3, 0x7D, 0x3F);
  	  $textcolor4 = imagecolorallocate($image, 0xE3, 0x5A, 0x3D);

	  // draw random lines on canvas
	  for($i=0; $i < 6; $i++) {
	    imagesetthickness($image, rand(1,3));
	    imageline($image, 0, rand(0,30), 120, rand(0,30), $linecolor);
	  }

	  // add random digits to canvas

	  $digit = '';
	  for($x = 15; $x <= 95; $x += 20) {
	    $digit .= ($num = rand(0, 9));
	    switch (rand(1,4))
	    {
		    case 1:
		    	$textcolor = $textcolor1;
		    	break;
	    	case 2:
	    		$textcolor = $textcolor2;
	        	break;
	    	case 3:
	    		$textcolor = $textcolor3;
	    		break;
	        case 4:
	        	$textcolor = $textcolor4;
	        	break;
	    }
	    imagechar($image, rand(3, 5), $x, rand(2, 14), $num, $textcolor);
	  }

	  $f3->set('SESSION.captcha', $digit);

	  //Create temporary stream with hashed name - this will never be shown to the user.

	  $filename = '/tmp/' . hash('md5',openssl_random_pseudo_bytes(128)) . '.png';

	  header('Content-type: image/png');
	  imagepng($image,$filename); //Write the image to the file. 

	  //Note - The following method outputs images using base64 encoding. In a live environment this may not be preferable due to the increase in memory usage overhead. 
	  $type = pathinfo($filename, PATHINFO_EXTENSION);
	  $data = file_get_contents($filename);
	  $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data); //Read the image again as base64... 

	  unlink($filename); //and clean up by deleting the previous file. 

	  return 'Copy the digits from the image into the following box:<br><br>' . '<p><img src="'.$base64.'" width="50%" height="20%" border="1" alt="CAPTCHA"></p>' . '<p><input type="numeric" class="form-control" id="' . $options . '" size="5" maxlength="5" name="' . $options . '" required><br>';

	}
}

?>
