<?php

class Controller {

	protected $layout = 'default';
	protected $template;

	public function __construct() {
		$f3=Base::instance();
		$this->f3 = $f3;

		// Connect to the database
		$this->db = new Database();
		$this->Model = new Model($this);

		//Load helpers
		$helpers = array('Auth', 'Log', 'Csrf');
		foreach($helpers as $helper) {
			$helperclass = $helper . "Helper";
			$this->$helper = new $helperclass($this);
		}
	}

	public function beforeRoute($f3) {

		$this->request = new Request();

		//Check user
		$this->Auth->resume();

		//Load settings
		$settings = Settings::getSettings();
		$settings['base'] = $f3->get('BASE');
		
		//Append debug mode to title
		if($settings['debug'] == 1) { $settings['name'] .= ' (Debug Mode)'; }

		$settings['path'] = $f3->get('PATH');
		$this->Settings = $settings;
		$f3->set('site',$settings);

                //Enable backwards compatability
                if($f3->get('PARAMS.*')) { $f3->set('PARAMS.3',$f3->get('PARAMS.*')); }

		//Extract request data
		extract($this->request->data);


		//Process before route code
		if(isset($beforeCode)) {
			Settings::process($beforeCode);
		}

	}

	public function afterRoute($f3) {	
		//Set page options
		$f3->set('title',isset($this->title) ? $this->title : get_class($this));

		//Prepare default menu	
		$f3->set('menu',$this->defaultMenu());

		//Setup user
		$f3->set('user',$this->Auth->user());

		//Check for admin
		$admin = false;
		if(stripos($f3->get('PARAMS.0'),'admin') !== false) { $admin = true; }

		//Identify action
		$controller = get_class($this);
		if($f3->exists('PARAMS.action')) {
			$action = $f3->get('PARAMS.action');	
		} else {
			$action = 'index';
		}

		//Handle admin actions
		if ($admin) {
			$controller = str_ireplace("Admin\\","",$controller);
			$action = "admin_$action";
		}

		//Handle errors
		if ($controller == 'Error') {
			$action = $f3->get('ERROR.code');
		}

		//Handle custom view
		if(isset($this->action)) {
			$action = $this->action;
		}

		//Extract request data
		extract($this->request->data);

		//Generate content
		$template = "$controller/$action.htm";
		if(isset($this->template)) {
			if(preg_match('!/!',$this->template)) {
				$template = $this->template . ".htm";
			} else {
				$template = "$controller/" . $this->template . ".htm";
			}
		}
		$content = View::instance()->render($template);
		$f3->set('content',$content);

		//Process before route code
		if(isset($afterCode)) {
			Settings::process($afterCode);
		}

		//Render template
		echo View::instance()->render($this->layout . '.htm');

		//Store the current page ([from] parameter) in session variable to prevent open-redirect injection in futher functions.
		//Ensure we don't set the SESSION.from to /user/login and cause a cycle. 
		//We probably don't want to redirect the user to the registration page after a successfull signup+login 
		//The SESSION.from variable is copied to the new session in authhelper.php setupSession(); 
		if($f3->get('PATH') != '/user/login' && $f3->get('PATH') != '/user/add' && $f3->get('PATH') != '/selftest')  //Do not reroute during testing (creates a cycle)
		{
			$f3->set('SESSION.from',$f3->get('PATH'));
		}	
	}

	public function defaultMenu() {
		$menu = array(
			array('label' => 'Search', 'link' => 'blog/search'),
			array('label' => 'Contact', 'link' => 'contact'),
		);

		//Load pages from database
		$pages = $this->Model->Pages->fetchAll();	
		
		foreach($pages as $page) {
			$menu[] = array('label' => $page->pagetitle, 'link' => 'page/display/' . $page->pagetitle);
		}

		//Add admin menu items
		if ($this->Auth->isAdmin()) {
			$menu[] = array('label' => 'Admin', 'link' => 'admin');
		}

		return $menu;
	}

}

?>
