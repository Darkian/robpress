<?php

class Page extends Controller {

	// Display page content by quering page content from DB, instead of files
	function display($f3) {
		$pagetitle = urldecode($f3->get('PARAMS.3'));

		// Query page info from DB
		$pageinfo = $this->Model->Pages->fetch(array('pagetitle' => $pagetitle));
	
		// Display page info	
		$f3->set('pagetitle',$pageinfo->pagetitle);
		$f3->set('page',$pageinfo->page);
	}
}
?>
