<?php
class Blog extends Controller {
	
	public function index($f3) {	
		if ($f3->exists('PARAMS.3')) {
			$categoryid = $f3->get('PARAMS.3');
			// CategoryId should be numeric.
			if(!is_numeric($categoryid)) {
				return errorRedirect($f3, 'Invalid Category Id');
			}
			$category = $this->Model->Categories->fetchById($categoryid);
			$postlist = array_values($this->Model->Post_Categories->fetchList(array('id','post_id'),array('category_id' => $categoryid)));
			$posts = $this->Model->Posts->fetchAll(array('id' => $postlist, 'published' => 'IS NOT NULL'),array('order' => 'published DESC'));
			$f3->set('category',$category);
		} else {
			$posts = $this->Model->Posts->fetchPublished();
		}
		$blogs = $this->Model->map($posts,'user_id','Users');
		$blogs = $this->Model->map($posts,array('post_id','Post_Categories','category_id'),'Categories',false,$blogs);
		$f3->set('blogs',$blogs);
	}

	public function view($f3) {
		$id = $f3->get('PARAMS.3');
		// PostId should be numeric and not empty.
		if(empty($id) || !is_numeric($id)) {
			return errorRedirect($f3, "Invalid Post Id");
		}
		$post = $this->Model->Posts->fetchById($id);
		if(empty($post)) {
			return errorRedirect($f3, 'Blog id '.$id.' not found');
		}
		/** check if the user has administrative access or post owner to display the unpublished post */
		if(is_null($post['published']) && !$this->Auth->isAdmin() && $this->Auth->user('id') != $post->user_id) {
			return errorRedirect($f3, 'Unauthorized');
		}
		
		$blog = $this->Model->map($post, 'user_id','Users');
		$blog = $this->Model->map($post, array('post_id','Post_Categories','category_id'), 'Categories', false, $blog);

		$comments = $this->Model->Comments->fetchAll(array('blog_id' => $id));
		$allcomments = $this->Model->map($comments, 'user_id', 'Users');

		$f3->set('comments', $allcomments);
		$f3->set('blog', $blog);		
	}

	public function reset($f3) {
		// Only admin should have access to this functionality
		if(!$this->Auth->isAdmin()) {
			return errorRedirect($f3, 'Access Denied');
		}
		$allposts = $this->Model->Posts->fetchAll();
		$allcategories = $this->Model->Categories->fetchAll();
		$allcomments = $this->Model->Comments->fetchAll();
		$allmaps = $this->Model->Post_Categories->fetchAll();
		foreach($allposts as $post) $post->erase();
		foreach($allcategories as $cat) $cat->erase();
		foreach($allcomments as $com) $com->erase();
		foreach($allmaps as $map) $map->erase();
		StatusMessage::add('Blog has been reset');
		return $f3->reroute('/');
	}

	public function comment($f3) {
		$id = $f3->get('PARAMS.3');
		// CommentId should be numeric
		if(!is_numeric($id)) {
			return errorRedirect($f3, 'Invalid Comment Id');
		}
		$post = $this->Model->Posts->fetchById($id);
		if($this->request->is('post')) {
			$redirectUrl = '/blog/view/'.$id;
			// logic for server side protection for commenting
			if($this->Settings['comments'] == false) {
				StatusMessage::add('Commenting is disabled', 'danger');
				return $f3->reroute($redirectUrl);
			}
			// Check CSRF
			if(!$this->Csrf->verifyToken()) {
				return $f3->reroute($redirectUrl);
			}
			$comment = $this->Model->Comments;
			$comment->copyfrom('POST');
			$comment->blog_id = $id;
			$comment->created = mydate();
			// Use user id from trusted source to prevent parameter manipulation 
			$comment->user_id = $this->Auth->user('id');

			//Moderation of comments
			// always enable all comment from admin
			// if comment moderation is on default moderate to 0
			if($this->Settings['moderate'] == 1 && !$this->Auth->isAdmin()) {
				$comment->moderated = 0;
			} else {
				$comment->moderated = 1;
			}

			//Default subject
			if(empty($this->request->data['subject'])) {
				$comment->subject = 'RE: ' . $post->title;
			}

			$comment->save();

			//Redirect
			if($comment->moderated == 0) {
				StatusMessage::add('Your comment has been submitted for moderation and will appear once it has been approved','success');
			} else {
				StatusMessage::add('Your comment has been posted','success');
			}
			return $f3->reroute('/blog/view/' . $id);
		}
	}

	public function moderate($f3) {	
		list($id,$option) = explode("/", $f3->get('PARAMS.3'));
		// CommentId and Options should be numeric
		if(!is_numeric($id) || !is_numeric($option)) {
			return errorRedirect($f3, 'Invalid Parameters');
		}
		$comments = $this->Model->Comments;
		$comment = $comments->fetchById($id);
		$post_id = $comment->blog_id;
		// Check CSRF
		if(!$this->Csrf->verifyToken()) {
			return $f3->reroute('/blog/view/'.$post_id);
		}

		// Only admin should be able to moderate comment
		if(!$this->Auth->isAdmin()) {
			return $f3->reroute('blog/view/'.$post_id);
		}

		//Approve
		if ($option == 1) {
			$comment->moderated = 1;
			$comment->save();
		} else {
		//Deny
			$comment->erase();
		}
		StatusMessage::add('The comment has been moderated');
		$f3->reroute('/blog/view/' . $post_id);
	}

	public function search($f3) {
		if($this->request->is('post')) {
			extract($this->request->data);
			$f3->set('search', $search);
			// Check CSRF
			if(!$this->Csrf->verifyToken()) {
				return $f3->reroute('/blog/search');
			}	
			//Get search results
			$search = str_replace("*","%",$search); //Allow * as wildcard
			//Use parameterized statement instead of string concatenation to prevent sql injection
			$statement = "SELECT id FROM `posts` WHERE `title` LIKE :s OR `content` LIKE :s";
			$ids = $this->db->connection->exec($statement, array(':s' => '%'.$search.'%'));
			$ids = Hash::extract($ids, '{n}.id');
			if(empty($ids)) {
				StatusMessage::add('No search results found for ' . h($search)); 
				return $f3->reroute('/blog/search');
			}

			//Load associated data
			$posts = $this->Model->Posts->fetchAll(array('id' => $ids));
			$blogs = $this->Model->map($posts, 'user_id','Users');
			$blogs = $this->Model->map($posts, array('post_id', 'Post_Categories', 'category_id'), 'Categories', false, $blogs);

			$f3->set('blogs', $blogs);
			$this->action = 'results';	
		}
	}
}
?>
