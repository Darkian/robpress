<?php
class User extends Controller {

	public function view($f3) {
		$userid = $f3->get('PARAMS.3');
		if(empty($userid) || !is_numeric($userid)) {
			return errorRedirect($f3, 'Invalid User Id');
		}
		$u = $this->Model->Users->fetchById($userid);
		// show draft post only when isAdmin or isOwner
		$condition = array('user_id' => $userid);
		if(!$this->Auth->isAdmin() && $this->Auth->user('id') != $userid) {
			$condition = array_merge($condition, array('published' => 'IS NOT NULL'));
		}
		$articles = $this->Model->Posts->fetchAll($condition);
		$comments = $this->Model->Comments->fetchAll(array('user_id' => $userid));

		$f3->set('u',$u);
		$f3->set('articles',$articles);
		$f3->set('comments',$comments);
	}

	public function add($f3) {
		if($this->request->is('post')) {
			$settings = $this->Model->Settings;
			$debug = $settings->getSetting('debug');

			if($debug == 0) {
				extract($this->request->data);

				//Check the captcha first - prevents bots from enumerating existing db users
				if($f3->exists('SESSION.captcha') && !empty($captcha)) 
				{
					if($f3->get('SESSION.captcha') != $captcha) 
					{
						StatusMessage::add('Registration failed: wrong CAPTCHA','danger');
						return $f3->reroute('/user/add');
					} else 
					{
						$formValid = true; //Captcha entered successfully
					}
				} else 
				{
					//The Captcha code was not saved within the session variable
					StatusMessage::add('Error while retrieving CAPTCHA: registration failed', 'danger');
					return $f3->reroute('/user/login');
				}


				//-- Validate a users username, displayname and email --//
				//Prevents a user submitting a form containing null fields or html tags. 
				$check = $this->Model->Users->fetch(array('username' => $username));
				$accountInfoTests = array(testInput("Username",$username),testInput("Display name",$displayname),testInput("Email",$email)); 
				foreach($accountInfoTests as $test) //Testing the username, display name and email
				{
					if(isset($test['msg']))
					{
						StatusMessage::add($test["msg"],'danger'); //If testInput() found null input or html tags, it will set appropriate danger message
						$formValid = false;
					}
				}

				//Validate Email format using php filter
				$email = filter_var($email, FILTER_SANITIZE_EMAIL);
				if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
					StatusMessage::add('Email format incorrect','danger');	
				 	$formValid = false;
				}

				if(!empty($check)) {
					StatusMessage::add('User already exists','danger');
					$formValid = false;
				} 
				//else if($password != $password2) {
				if($password != $password2) {
					StatusMessage::add('Passwords must match','danger');
					$formValid = false;
				}
			       	// Check if form is valid for crsf token.	
				if(!$this->Csrf->verifyToken()) {
					$formValid = false;
				}//else {
				if($formValid == true) //If no errors were thrown, attempt to register the user. 
				{
					$user = $this->Model->Users;
					$user->copyfrom('POST');
					$user->created = mydate();
					$user->bio = '';
					$user->level = 1;
					
					//Set the users password
					$user->setPassword($password);
					
					if(empty($displayname)) {
						$user->displayname = $user->username;
					}

					$user->save();	
					StatusMessage::add('Registration successfully completed','success');
					return $f3->reroute('/user/login');
				}
			} else {
				extract($this->request->data);
				$check = $this->Model->Users->fetch(array('username' => $username));
				$user = $this->Model->Users;
				$user->copyfrom('POST');
				$user->created = mydate();
				$user->bio = '';
				$user->level = 1;
				
				//Set the users password
				$user->setPassword($password);
				
				if(empty($displayname)) {
					$user->displayname = $user->username;
				}

				$user->save();	
				StatusMessage::add('Registration complete','success');
				return $f3->reroute('/user/login');
			}
		}
	}

	public function login($f3) {
		/** YOU MAY NOT CHANGE THIS FUNCTION - Make any changes in Auth->checkLogin, Auth->login and afterLogin() */
		if ($this->request->is('post')) {

			//Check for debug mode
			$settings = $this->Model->Settings;
			$debug = $settings->getSetting('debug');

			//Either allow log in with checked and approved login, or debug mode login
			list($username,$password) = array($this->request->data['username'],$this->request->data['password']);
			if (	($this->Auth->checkLogin($username,$password,$this->request,$debug) && ($this->Auth->login($username,$password))) ||
				($debug && $this->Auth->debugLogin($username))) {

					$this->afterLogin($f3);

			} else {
				StatusMessage::add('Invalid username or password','danger');
			}
		}		
	}

	/* Handle after logging in */
	private function afterLogin($f3) {
				StatusMessage::add('Logged in successfully','success'); //Why does this not show up ?

				//Redirect to where they came from
				/*if(isset($_GET['from'])) {
					//die($_GET['from']);
					$f3->reroute($_GET['from']);
				} else {
					$f3->reroute('/');	
				}*/

				//Get the previously visited site from the 'SESSION.from' hive variable. note: We copy this value over before calling session_destroy() in authhelper setupSession()
				// TEST This (Auth bypass) - The user should not be able to redirect into an admin area. 
				if($f3->exists('SESSION.from'))
				{
					$f3->reroute($f3->get('SESSION.from'));
				}
				else
				{
					$f3->reroute('/');
				}
	}

	public function logout($f3) {
		// Check CSRF
		if(!$this->Csrf->verifyToken()) {
			return $f3->reroute('/');
		}
		$this->Auth->logout();
		StatusMessage::add('Logged out successfully','success');
		$f3->reroute('/');	
	}


	public function profile($f3) {	
		$id = $this->Auth->user('id');
		extract($this->request->data);
		$u = $this->Model->Users->fetchById($id);
		$oldpass = $u->password;
		if($this->request->is('post')) {
			// Check CSRF
			if(!$this->Csrf->verifyToken()) {
				return $f3->reroute('/user/profile');
			}
			$u->copyfrom('POST');

			//** Validate Display name (check if null or contains html chars) **//
			$testResult = testInput('Display Name', $u->displayname);
			if(isset($testResult['msg']))
			{
				StatusMessage::add($testResult["msg"],'danger');
			} else {
				
				if(empty($u->password)) { // if there is no password, update the old password to DB 
					$u->password = $oldpass; 
				} else { // if there is a new password, update the new password to DB
					$u->setPassword($u->password);	
				}

				//Handle avatar upload
				if(isset($_FILES['avatar']) && isset($_FILES['avatar']['tmp_name']) && !empty($_FILES['avatar']['tmp_name'])) {
					$url = File::Upload($_FILES['avatar']);
					$u->avatar = $url;
				} else if(isset($reset)) {
					$u->avatar = '';
				}

				$u->save();
				\StatusMessage::add('Profile updated successfully','success');
				return $f3->reroute('/user/profile');
			}
		}			
		$_POST = $u->cast();
		$f3->set('u',$u);
	}

	public function promote($f3) {
		// This functionality is fishy, why does user have an option to promote him/her self as admin
		// and why does admin need to promote him/her self as admin
		// ** original requirement may be have a functionality to promote someone by an admin **
		// however, there is no reference for this function at all. so **** consult site owner about the requirement **** 
		/*
		$id = $this->Auth->user('id');
		$u = $this->Model->Users->fetchById($id);
		$u->level = 2;
		$u->save();
		*/
		return $f3->reroute('/');
	}

}
?>
