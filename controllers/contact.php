<?php
class Contact extends Controller {
	public function index($f3) {
		if($this->request->is('post')) 
		{
			// Check for csrf
			if(!$this->Csrf->verifyToken()) {
				return $f3->reroute('/contact');
			}
			extract($this->request->data);
			if(empty($captcha) || $f3->get('SESSION.captcha') != $captcha)
			{
				StatusMessage::add("Please enter the text from the captcha into the box.", "danger");
			}
			else if(!$f3->exists('SESSION.captcha'))
			{
				StatusMessage::add("There was an issue getting the Captcha code from the server, please cotact system administrator", "danger");
			}	
			else //Captcha entered successfully. 
			{
				//Hardcode $to address from site settings- removed field from form in contact.php.  
				//todo: Introduce CAPTCHA in contact.php to prevent abuse.
				$to = $this->Model->Settings->getSetting('email'); //The site settings email
				$from = $from; //Get new email if changed
				
				$message = c($message);
				$subject = c($subject);

				$from = "From: $from";

				mail($to,$subject,$message,$from);

				StatusMessage::add('Thank you for contacting us');
				return $f3->reroute('/');
			}
		}	
		$from = $this->Auth->user('email'); //The users email

	}
}

?>
