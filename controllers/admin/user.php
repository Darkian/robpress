<?php

namespace Admin;

class User extends AdminController {

	public function index($f3) {
		$users = $this->Model->Users->fetchAll();
		$f3->set('users',$users);
	}

	public function edit($f3) {	
		$id = $f3->get('PARAMS.3');
		// User Id should not be empty and should be numeric
		if(empty($id) || !is_numeric($id)) {
			return errorRedirect($f3, 'Invalid User Id');
		}
		$u = $this->Model->Users->fetch($id);
		$safeUsername = $u->username;//This additional vaiable keeps track of the original username; incase admin attempts to submit disallowed fields. The form will be repopulated with original safe username.

		// Keep the old password for updating DB in the case of no password changing 
		$oldPassword = $u->password;
		// Set $u->password to empty to avoid display hash value of password
		$u->password = "";

		// Process POST request
		if($this->request->is('post')) {
			// Check CSRF
			if(!$this->Csrf->verifyToken()) {
				return $f3->reroute('/admin/user');
			}
			$u->copyfrom('POST');
			$formValid = true; //Only save() user is form is valid
			$testArray = array(testInput("Username", $this->request->data['username']), testInput("Display name",$this->request->data['displayname']));
			foreach($testArray as $arr)
			{
				if(isset($arr['msg']))
				{
					\StatusMessage::add($arr['msg'],'danger');
					$formValid = false;
				}
			}

			if($formValid==true)
			{
				if(empty($this->request->data['password'])) { // if the password field is empty, update password by using old password.
					$u->password = $oldPassword;	
				} else { // if the password field isn't empty, replace the password with new hashed password.
					$u->setPassword($this->request->data['password']);
				}	
	
				$u->save();
			\StatusMessage::add('User updated successfully','success');
			return $f3->reroute('/admin/user');
			}
		}
			$_POST = $u->cast();
			$f3->set('u',$u);
			// --- Show the safe username ---
			$f3->set('u->username',$safeUsername);
	}

	public function delete($f3) {
		$id = $f3->get('PARAMS.3');
		$u = $this->Model->Users->fetch($id);
		// Check CSRF
		if(!$this->Csrf->verifyToken()) {
			return $f3->reroute('/admin/user');
		}
		if($id == $this->Auth->user('id')) {
			\StatusMessage::add('You cannot remove yourself','danger');
			return $f3->reroute('/admin/user');
		}

		//Remove all posts and comments
		$posts = $this->Model->Posts->fetchAll(array('user_id' => $id));
		foreach($posts as $post) {
			$post_categories = $this->Model->Post_Categories->fetchAll(array('post_id' => $post->id));
			foreach($post_categories as $cat) {
				$cat->erase();
			}
			$post->erase();
		}
		$comments = $this->Model->Comments->fetchAll(array('user_id' => $id));
		foreach($comments as $comment) {
			$comment->erase();
		}
		$u->erase();

		\StatusMessage::add('User has been removed','success');
		return $f3->reroute('/admin/user');
	}


}

?>
