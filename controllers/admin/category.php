<?php

	namespace Admin;

	class Category extends AdminController {

		public function index($f3) {
			$categories = $this->Model->Categories->fetchAll();
			$counts = array();
			foreach($categories as $category) {
				$counts[$category->id] = $this->Model->Post_Categories->fetchCount(array('category_id' => $category->id));
			}
			$f3->set('categories',$categories);
			$f3->set('counts',$counts);
		}

		public function add($f3) {
			if($this->request->is('post')) {
				$category = $this->Model->Categories;
				// Check CSRF
				if(!$this->Csrf->verifyToken()) {
					return $f3->reroute('/admin/category');
				}
				$category->title = $this->request->data['title'];
				$category->save();

				\StatusMessage::add('Category added successfully','success');
				return $f3->reroute('/admin/category');
			}
		}

		public function delete($f3) {
			// Check CSRF
			if(!$this->Csrf->verifyToken()) {
				return $f3->reroute('/admin/category');
			}
			$categoryid = $f3->get('PARAMS.3');
			// Category Id should not be empty and should be numeric
			if(empty($categoryid) || !is_numeric($categoryid)) {
				return errorRedirect($f3, 'Invalid Category Id', '/admin/category');
			}
			$category = $this->Model->Categories->fetchById($categoryid);
			$category->erase();

			//Delete links		
			$links = $this->Model->Post_Categories->fetchAll(array('category_id' => $categoryid));
			foreach($links as $link) { $link->erase(); } 
	
			\StatusMessage::add('Category deleted successfully','success');
			return $f3->reroute('/admin/category');
		}

		public function edit($f3) {
			$categoryid = $f3->get('PARAMS.3');
			// Category Id should not be empty and should be numeric
			if(empty($categoryid) || !is_numeric($categoryid)) {
				return errorRedirect($f3, 'Invalid Category Id', '/admin/category');
			}
			$category = $this->Model->Categories->fetchById($categoryid);
			if($this->request->is('post')) {
				// Check CSRF
				if(!$this->Csrf->verifyToken()) {
					return $f3->reroute('/admin/category');
				}
				$category->title = $this->request->data['title'];
				$category->save();
				\StatusMessage::add('Category updated successfully','success');
				return $f3->reroute('/admin/category');
			}
			$f3->set('category',$category);
		}


	}

?>
