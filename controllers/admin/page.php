<?php

namespace Admin;

class Page extends AdminController {

	/** Loads all page titles from DB **/
	public function index($f3) {
		$pages = $this->Model->Pages->fetchAll();
		$pagetitles = array();
		
		// Gets pages' info from DB	
		foreach ($pages as $page) {
			$pagetitles[$page->pagetitle] = $page->pagetitle;	
		}

		$f3->set('pages',$pagetitles);
	}

	/** Add a page title to DB **/
	public function add($f3) {
		if($this->request->is('post')) {
			// Check CSRF
			if(!$this->Csrf->verifyToken()) {
				return $f3->reroute('/admin/page');
			}

			$pagetitle = $this->request->data['title'];
			$page = $this->Model->Pages;
			$page->pagetitle = $pagetitle;

			// Check whether page title is empty
			if(empty($page->pagetitle)) {
				$errors = 'You did not specify a page title';
			}else
			{
				//Check if the page exists first
				$existing_page = $this->Model->Pages->fetch(array('pagetitle' => $pagetitle));
				if(!empty($existing_page))
				{
					$errors = 'A page alread exists with that name';
				}
			}

			if ($errors) { // display error if there is no page title
				\StatusMessage::add($errors,'danger');
				return $f3->reroute('/admin/page/');
			} else { // display the page added
				$page->save();
				\StatusMessage::add('Page created successfully','success');
				return $f3->reroute('/admin/page/edit/' . $pagetitle);
			}

		}
	}

	/** Modify content in DB  **/
	public function edit($f3) {
		$pagetitle = $f3->get('PARAMS.3');
		
		// Check CSRF
		if ($this->request->is('post')) {
			if(!$this->Csrf->verifyToken()) {
				return $f3->reroute('/admin/page');
			}
		}
		
		// Query page info from DB
		$page = $this->Model->Pages->fetch(array('pagetitle' => $pagetitle));
		
		// Modify Content in DB, if there is a POST request
		if ($this->request->is('post')) {
			$page->page = $this->request->data['content'];
			$page->save();

			\StatusMessage::add('Page updated successfully','success');
			return $f3->reroute('/admin/page');
		}

		// Display page /admin/page/edit
		$f3->set('pagetitle',$page->pagetitle);
		$f3->set('page',$page->page);
	}

	/** Delete page from DB **/
	public function delete($f3) {
		// Check CSRF
		if(!$this->Csrf->verifyToken()) {
			return $f3->reroute('/admin/page');
		}

		$pagetitle = $f3->get('PARAMS.3');

		// Delete page from DB
		$pages = $this->Model->Pages->fetch(array('pagetitle' => $pagetitle));	
		$pages->erase();	

		// Go back to /admin/page
		\StatusMessage::add('Page deleted successfully','success');
		return $f3->reroute('/admin/page');	
	}

}

?>
