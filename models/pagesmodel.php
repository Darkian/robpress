<?php

class PagesModel extends GenericModel {

	// This function created for test only
	public function delete($title) {
		$page = $this->fetch(array('pagetitle'=> $title));
		if(empty($page)) 
		{
			return false;
		}
		$page->erase();
		return true;
	}

}

?>
