<?php

class UsersModel extends GenericModel {

	/** Update the password for a user account */
	public function setPassword($password) {
		$pool = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.";
		$salt = '';
		
		/** generate a salt value (9 alphanumberic characters) **/
		for ($i=0; $i < 9; $i++) {
			$salt .= $pool[mt_rand(0,62)];
		}

		// hash the password by using sha256 algorithm with a salt 	
		$password = $salt."$".hash('sha256',$salt.$password);

		$this->password = $password;
	}

}

?>
