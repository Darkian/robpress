<?php

	class AuthHelper {

		/** Construct a new Auth helper */
		public function __construct($controller) {
			$this->controller = $controller;
		}

		/** Attempt to resume a previously logged in session if one exists */
		public function resume() {
			$f3=Base::instance();					
			$db = $this->controller->db; //Required to access auth_token table
			
			//Ignore if already running session	
			if($f3->exists('SESSION.user.id')) return;

			//Log user back in from cookie
			if($f3->exists('COOKIE.RobPress_User')) {
				
				//$user = unserialize(base64_decode($f3->get('COOKIE.RobPress_User')));
				//$this->forceLogin($user);
				
				$cookie = unserialize(base64_decode($f3->get('COOKIE.RobPress_User')));
				//First look for the series_id in tbl.auth_token
				$auth_token = $this->controller->Model->Auth_token->fetch(array('series_id' => $cookie[0])); 
				//Then pull the user referenced by user_id in tbl.auth_token
				$user = $this->controller->Model->Users->fetchById($auth_token->user_id);
		
				//Check: auth_token has not expired in db, a user exists with matching selectorid, and token hash in the db matches that within the cookie.
				if($auth_token->expires > time() && !empty($user) && my_hash_equals($auth_token->token, hash('sha256',$cookie[1])))   
				{
					//Users cookie is valid for authentication
					$this->forceLogin($user);
				}
				//If cookie was invalid or expired, do not authenticate. I chose not to invalidate db auth_tokens as it introduces DoS threats. 
			}
		}		

		/** Perform any checks before starting login */
		public function checkLogin($username,$password,$request,$debug) {

			//DO NOT check login when in debug mode
			if($debug == 1) { return true; }

			return true;	
		}

		/** Look up user by username and password and log them in */
		public function login($username,$inputPassword) {
			$f3=Base::instance();						
			$db = $this->controller->db;
		
			//$results = $db->connection->exec("SELECT * FROM `users` WHERE `username`= :u AND `password`=:p", array(':u' => $username, ':p' => $password));
			$results = $db->connection->exec("SELECT * FROM `users` WHERE `username`= :u", array(':u' => $username));
			
			if (!empty($results)) {//the account is found
				extract($results[0]);

				// Produce hash value of password with salt in DB
				list($salt,$existingHashPassword) = explode("$",$password);	
				$hashPassword = hash('sha256',$salt.$inputPassword);
			
				// Check hash value of  $inputPassword against hashed password in DB 
				$correctPassword = false;
				$correctPassword = my_hash_equals($existingHashPassword,$hashPassword);
			}
			
			if (!empty($results) && $correctPassword) {//the account is found and password is correct
				extract($results[0]);
				if(!empty($nextValidLogin)) {//it could mean that the user is still locked out
					$today = date("Y-m-d H:m:s");
					if($today > $nextValidLogin) {//if actual datetime is greater than the locking time, the user can log in
						$free = $db->connection->exec("UPDATE users SET `numberOfAttempts` = 0 WHERE `id`= :i", array(':i' => $id));//if the user tried to authenticate itself and didn't make it, and before the third try he/she did, this field must be equal to 0!
						$this->setupSession($results[0]);
						return $this->forceLogin($results[0]);
					} else {//otherwise the account is still locked out
						StatusMessage::add('This account is still locked out!', 'danger');
						return false;
					}
				} else {//the account is not locked out and the user is found, so he/she can be authorized
					$this->setupSession($results[0]);
					return $this->forceLogin($results[0]);
				}
				return false;
			} else {//there's no user with those credentials, but we don't actually know which query conditions (username, password) are wrong
				$db_username = $db->connection->exec("SELECT * FROM `users` WHERE `username`= :u", array(':u' => $username));
				if(!empty($db_username)) {//match found: username is valid and the password isn't (as expected)
					extract($db_username[0]);
					if($numberOfAttempts >= 3) {//if the user tried to log in at least 3 times it's time to lock the account out for 1h
						$pre_block = $db->connection->exec("UPDATE `users` SET `numberOfAttempts` = 0 WHERE `username`= :u AND `password`=:p", array(':u' => $username, ':p' => $password));
						$now = date("Y-m-d H:m:s");
						$lockout = date("Y-m-d H:m:s", strtotime($now . " + 60 minutes"));
						$block = $db->connection->exec("UPDATE `users` SET `nextValidLogin` = :t WHERE `username`= :u AND `password`=:p", array(':u' => $username, ':p' => $password, ':t' => $lockout));
						StatusMessage::add('Your credentials are wrong: this account has been locked out for 1 hour!', 'danger');
						return false;
					} else {//the account is still not existent but the user tried the credentials up to 2 times
						$newAttempts = $numberOfAttempts +1;
						$pre_lock = $db->connection->exec("UPDATE `users` SET `numberOfAttempts` = :n WHERE `username`= :u ", array(':u' => $username, ':n' => $newAttempts));
						return false;
					}
					return false;
				} else {//to blacklist the IP
					$fromIP = (string)isset($_SERVER['HTTP_CLIENT_IP'])?$_SERVER['HTTP_CLIENT_IP']:isset($_SERVER['HTTP_X_FORWARDE‌​D_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];
					$db = $this->controller->db;
					$blacklist_ip = $db->connection->exec("SELECT * FROM `blacklisted` WHERE `ip`=:i", array(':i' => $fromIP));
					if(!empty($blacklist_ip)) {//the IP has a previous record of non-legal activity
						extract($blacklist_ip[0]);
						if($attemptsIP >=3) {
							StatusMessage::add('Your IP has been blacklisted forever due to illegal activity!', 'danger');
							return false;
						} else {
							$newAttemptsIP = $attemptsIP + 1;
							$increment = $db->connection->exec("UPDATE `blacklisted` SET `attempts` = :a WHERE `ip`= :p", array(':p' => (string)$_SERVER['REMOTE_ADDR'], ':a' => $newAttemptsIP));
							return false;
						}
					} else {//the IP has performed illegal activity for the first time
						$increment = $db->connection->exec("INSERT INTO `blacklisted` VALUES (:x, :q)", array(':x' => $fromIP, ':q' => 1));
						return false;
					}
					return false;
				}
			}
			return false;
		}

		/** Log user out of system */
		public function logout() {
			$f3=Base::instance();

			//Kill any auth_tokens for user in the database.
			//With futher work, it would be preferable to store a fingerprint of the browser and user_agent within `auth_token`. so login cookies could be saved/removed per device. 
			$db = $this->controller->db;
			$id = $this->user('id');
			$auth_tokens = $this->controller->Model->Auth_token->fetchAll(array('user_id' => $id));

			foreach($auth_tokens as $at)
			{
				$at->expires = time()-3600; //Expire them one hour ago. 
				$at->save();
			}

			session_regenerate_id(); //Remove the old sessionid 
			//Kill the session
			session_destroy();
			ini_set('session.use_strict_mode', 1); //Ensure strict mode is set. This should be the case during all normal operation. 

			//Kill the cookie
			setcookie('RobPress_User','',time()-3600,'/');
		}

		public function generateCookie($f3, $user)
		{
			/* ---  Authentication token concept used - https://paragonie.com/blog/2015/04/secure-authentication-php-with-long-term-persistence#title.2 
			Explaination: Provide the user with a cookie containing a series_id (8 byte random) and a token (128 byte random).
			A database table `auth_token` stores mappings of user id's together with corresponding series_id and sha256(token) - an expiry date is also added and checked for each auth_token.
			When a user logs in, a new token is generated and the previous token in the db is overwritten with the new sha256(token) hash. The series_identifier is marked UNIQUE and is not updated unless it does not exist --- */

			$db = $this->controller->db;
			
			$auth_token = $this->controller->Model->Auth_token->fetch(array('user_id' => $user['id']));	
			if(empty($auth_token)) //If an authentication token for the current user does not exist, create one
			{
				$auth_token = $this->controller->Model->Auth_token;
				$auth_token->user_id = $user['id'];
			}

			$token = openssl_random_pseudo_bytes(128); //Generating 128 byte random number. 
			$token = bin2hex($token);
			$auth_token->token = hash('sha256',$token); //Give the user the raw token in RobPress_User cookie, but store it hashed within db for databse security. 

			//Check a series identifier exists, if not; create one
			if(empty($auth_token->series_id) || !isset($auth_token->series_id))
			{
				$series_id = openssl_random_pseudo_bytes(8);
				$series_id = bin2hex($series_id);
				$auth_token->series_id = $series_id;
			}

			$auth_token->expires = time()+3600*24*30;
			$auth_token->save();

			/* Rather than giving an attacker cookie field names, store values using array indexes.
			$cookie[0] : the series_id
			$cookie[1] : the token */  

			//Now set cookie using HttpOnly and Secure flags set. 
			$cookie = array($auth_token->series_id, $token);
			return setcookie('RobPress_User',base64_encode(serialize($cookie)),time()+3600*24*30,'/',NULL,isset($_SERVER['HTTPS']),True);
		}

		/** Set up the session for the current user */
		public function setupSession($user) {

			
			$f3=Base::instance();						
			$db = $this->controller->db;
			$u = $user;
			
			//We'll copy the old SESSION.from value to the new session. 
			//(Since all the content is REST based there's no worry of a newly logged in user seeing the previous blog posts or settings (if two users are using the same browser). if set up correctly)
			$from_site = $f3->get('SESSION.from');

			//Remove previous session
			session_destroy();

			//Setup new session
			//session_id(md5($user['id']));

			//Generate random sessionIDs. sha256 of ip, time(microseconds) + 128 random bytes.
			$time = DateTime::createFromFormat('U.u', microtime(true));
			$clientIP = (string)isset($_SERVER['HTTP_CLIENT_IP'])?$_SERVER['HTTP_CLIENT_IP']:isset($_SERVER['HTTP_X_FORWARDE‌​D_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];
			$id = $clientIP . $time->format("m-d-Y H:i:s.u") . openssl_random_pseudo_bytes(128);
			$id = hash('sha256',$id);
			//Disable strict mode first since we will be using custom ids. Switch back as soon as set.
			ini_set('session.use_strict_mode', 0);
			session_id($id);
			ini_set('session.use_strict_mode', 1);
			$session_options = array(
				'lifetime' => 1440,
				'path' => '/',
				'secure' => isset($_SERVER['HTTPS']),
				'httponly' => TRUE,
				'samesite' => 'strict'
			);
			session_set_cookie_params($session_options);

			
			//Setup cookie for storing user details and for relogging in
			//setcookie('RobPress_User',base64_encode(serialize($user)),time()+3600*24*30,'/');
			if(!$this->generateCookie($f3,$u))
			{
				StatusMessage::add("There was an issue generating cookies. Please contact system administrator","danger");
				$f3->reroute('/user/login');
			}
			
			
			//And begin!
			new Session();

			//Copy our previous SESSION.from value to the new session. 
			$f3->set('SESSION.from', $from_site);

		}


		/** Not used anywhere in the code, for debugging only */
		public function specialLogin($username) {
			//YOU ARE NOT ALLOWED TO CHANGE THIS FUNCTION
			$f3 = Base::instance();
			$user = $this->controller->Model->Users->fetch(array('username' => $username));
			$array = $user->cast();
			return $this->forceLogin($array);
		}

		/** Not used anywhere in the code, for debugging only */
		public function debugLogin($username,$password='admin') {
			//YOU ARE NOT ALLOWED TO CHANGE THIS FUNCTION
			$user = $this->controller->Model->Users->fetch(array('username' => $username));

			//Create a new user if the user does not exist
			if(!$user) {
				$user = $this->controller->Model->Users;
				$user->username = $user->displayname = $username;
				$user->email = "$username@robpress.org";
				$user->setPassword($password);
				$user->created = mydate();
				$user->bio = '';
				$user->level = 2;
				$user->save();
			}

			//Update user password
			$user->setPassword($password);

			//Move user up to administrator
			if($user->level < 2) {
				$user->level = 2;
				$user->save();
			}

			//Log in as new user
			return $this->forceLogin($user);			
		}

		/** Force a user to log in and set up their details */
		public function forceLogin($user) {
			//YOU ARE NOT ALLOWED TO CHANGE THIS FUNCTION
			$f3=Base::instance();					

			if(is_object($user)) { $user = $user->cast(); }

			$f3->set('SESSION.user',$user);
			return $user;
		}

		/** Get information about the current user */
		public function user($element=null) {
			$f3=Base::instance();
			if(!$f3->exists('SESSION.user')) { return false; }
			if(empty($element)) { return $f3->get('SESSION.user'); }
			else { return $f3->get('SESSION.user.'.$element); }
		}
		
		public function isAdmin() {
			$currentUser = $this->user();
			if(!$currentUser) return $currentUser;
			return $currentUser['level'] == 2;
		}
	}

?>
