<?php

	class LogHelper {

		/** Construct a new Log helper */
		public function __construct($controller) {
			$this->controller = $controller;
		}

		/** Log information to console.log **/
		public function consolelog($data) {
			if($this->controller->Settings['debug'] != 1) return;
			if(is_object($data) || is_array($data)) {
				$this->log(json_encode($data));				
			}
			else {
				$this->log('"'.$data.'"');
			}
		}

		private function log($data) {
			echo "<script>console.log(".$data.");</script>";	
		}
	}

?>
