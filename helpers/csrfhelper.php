<?php

	class CsrfHelper {

		/** Construct a new CSRF helper */
		public function __construct($controller = null) {
			$this->controller = $controller;
		}

		/** Generate token, save to session and return it back (to be insert to form) **/
		public function prepareToken() {
			$token = bin2hex(openssl_random_pseudo_bytes(32));
			if(empty($_SESSION['csrfToken'])) {
				$_SESSION['csrfToken'] = array();
			};
			array_push($_SESSION['csrfToken'], $token);
			return $token;
		}

		public function verifyToken() {
			// ignore token when in debug mode	
			if($this->controller->Settings['debug'] == 1) return true;
			$savedTokens = $_SESSION['csrfToken'];
			$token = $_SERVER['REQUEST_METHOD'] === 'POST' ? $_POST['csrfToken'] : $_GET['csrfToken'];
			// if there is none saved Tokens in the session reject.
			if(empty($savedTokens)) return false;
			// if there is a valid token in the saved token then remove it from
			// the saved token and return true
			// here can be improve to have a tuple between token and form to
			// prevent cross form token usage
			if(in_array($token, $savedTokens)) {
				foreach(array_keys($savedTokens, $token) as $key) {
					unset($savedTokens[$key]);
				}
				return true;
			}
			StatusMessage::add('System cannot process your request. Try again later.', 'danger');
			return false;	
		}
	}

?>
